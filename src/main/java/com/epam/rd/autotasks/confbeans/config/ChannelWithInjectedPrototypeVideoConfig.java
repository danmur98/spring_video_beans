package com.epam.rd.autotasks.confbeans.config;


import org.springframework.context.annotation.Configuration;



import com.epam.rd.autotasks.confbeans.video.Channel;
import com.epam.rd.autotasks.confbeans.video.Video;

import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Scope;

import java.time.LocalDateTime;

@Configuration
public class ChannelWithInjectedPrototypeVideoConfig {

    @Bean
    @Scope("prototype")
    public Video video() {
        return new Video("Cat Failure Compilation", LocalDateTime.now());
    }

    @Bean
    public Channel channel(Video video) {
        Channel channel = new Channel();
        for (int i = 1; i <= 365; i++) {
            channel.addVideo(video);
            video = new Video("Cat Failure Compilation", LocalDateTime.now());
        }
        return channel;
    }
}
