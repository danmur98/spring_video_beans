package com.epam.rd.autotasks.confbeans.config;


import org.springframework.context.annotation.Configuration;


import com.epam.rd.autotasks.confbeans.video.Channel;
import com.epam.rd.autotasks.confbeans.video.Video;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import java.time.LocalDateTime;


@Configuration
public class ChannelWithPhantomVideoStudioConfig {
    @Bean
    @Scope("prototype")
    public Video video() {
        return new Video("Cat & Curios", null);
    }

    @Bean
    public Channel channel(Video video) {
        Channel channel = new Channel();
        for (int i = 1; i <= 8; i++) {
            channel.addVideo(new Video(video.getName() + " " + i, LocalDateTime.of(2001, 10, 18, 10, 0).plusYears(i - 1)));
        }
        return channel;
    }

}
