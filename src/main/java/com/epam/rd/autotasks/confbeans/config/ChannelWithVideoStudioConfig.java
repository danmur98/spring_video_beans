package com.epam.rd.autotasks.confbeans.config;


import org.springframework.context.annotation.Configuration;


import com.epam.rd.autotasks.confbeans.video.Channel;
import com.epam.rd.autotasks.confbeans.video.Video;
import com.epam.rd.autotasks.confbeans.video.VideoStudio;
import org.springframework.context.annotation.Bean;

import java.time.LocalDateTime;

@Configuration
public class ChannelWithVideoStudioConfig {

    @Bean
    public VideoStudio videoStudio() {
        return new VideoStudio() {
            private int movieNumber = 1;

            @Override
            public Video produce() {
                Video video = new Video("Cat & Curios " + movieNumber, LocalDateTime.of(2001, 10, 18, 10, 0).plusYears(movieNumber - 1));
                movieNumber++;
                return video;
            }
        };
    }

    @Bean
    public Channel channel(VideoStudio videoStudio) {
        Channel channel = new Channel();
        for (int i = 1; i <= 8; i++) {
            channel.addVideo(videoStudio.produce());
        }
        return channel;
    }
}
